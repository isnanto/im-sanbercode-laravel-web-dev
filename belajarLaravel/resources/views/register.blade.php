<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrasi</title>
</head>
<body>   
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
    
        <label for="first">First Name :</label>
        <input type="text" name="first"> 
        <p>
        <label for="last">Last Name :</label>
        <input type="text" name="last">
        <p>
    
        </p>
    
        <label for="Gender">Gender:</label><br>  
            <input type="radio" name="gender" id="male" value="male"> Male <br>
            <input type="radio" name="gender" id="female" value="female "> Female<br>
            <input type="radio" name="gender" id="other" value="other">Other<br>
             
        <br>
        <label for="Nasionality">Nationality:</label><br>
        <select name="Nasionality" id="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
            <option value="Brunei">Brunei</option>         
        </select>
        <p></p>
        <label for="language">Language spoken:</label><br>
        <input type="checkbox" name="language" value="Indonesia">Indonesia <br>
        <input type="checkbox" name="language" value="English" >English<br>
        <input type="checkbox" name="language" value="other" >Other<br><br>
    
        <label for="Bio">Bio:</label><br><br>
        <textarea id="bio" name="bio"  rows="10" cols="50"></textarea><br>
        <input type="submit">
    </form>
</body>
</html>