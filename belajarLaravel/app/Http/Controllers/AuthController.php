<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register() {
        return view('register');
    }

    public function welcome(Request $request) {
       //dd($request->all());

        $fname = $request['first'];
        $lname = $request['last'];

        return view('sambutan', ['fname'=>$fname, 'lname'=>$lname]);
    }
}
