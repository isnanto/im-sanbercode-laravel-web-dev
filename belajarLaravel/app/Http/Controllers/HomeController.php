<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // 
    public function utama() {
        return view('isnanto');
    }

    public function biodata() {
        return view('halaman.biodata');
    }

    public function kirim (Request $request) {

        //dd($request->all());
        $nama = $request['nama'];
        $alamat = $request['alamat'];
        $jnsKelamin = $request['jnskelamin'];
        
        return view('halaman.home',['nama'=>$nama,'alamat'=>$alamat,'jnskelamin'=>$jnsKelamin]);
 
    }


    // mulai sesuai tugas
    public function index() {
        return view('home');
    }


}
